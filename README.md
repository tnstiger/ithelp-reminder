# iThome 鐵人賽 Slack 催稿雞

## 設定參賽者 profle
/profile.json
```
[
    {
        // 鐵人賽系列 ID
        id: "xxxx",

        // slack 的 member ID
        slack: "",
    }
]
```

## 設定 .env
```
1. cp .env.sample .env
2. 填寫 .env 內容
```

## 使用 PM2 launch
```
pm2 start app.js
```