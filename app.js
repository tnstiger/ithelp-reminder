require("dotenv").config();
const Parser = require("rss-parser");
const parser = new Parser();
const dayjs = require("dayjs");
const cron = require("node-cron");
const axios = require("axios");
const profiles = require("./profile.json");
let completed = false;

if (process.env.ENV == "production") {
  cron.schedule("0 12,18,21,22,23 * * *", async () => {
    await reminder();
  });
} else {
  (async () => {
    await reminder();
  })();
}

async function reminder() {
  let list = [];
  let posts = [];
  for await (profile of profiles) {
    let feed = await parser.parseURL(
      `https://ithelp.ithome.com.tw/rss/series/${profile.id}`
    );

    let finded = feed.items.find((o) =>
      o.pubDate.includes(dayjs().format("YYYY-MM-DD"))
    );
    if (!finded) {
      list.push(profile);
    } else {
      posts.push(finded);
    }
  }

  if (list.length) {
    completed = false;
    list = list.map((o) => `<@${o.slack}>`).join(" ");
    let payload = {
      channel:
        process.env.ENV == "production"
          ? `#${process.env.CHANNEL_PRODUCTION}`
          : `#${process.env.DEV}`,
      text: `凹嗚嗚嗚嗚嗚嗚嗚！${list} 快點交稿！快點交稿！快點交稿！`,
    };
    await axios.post(process.env.SLACK_WEBHOOK_URL, payload);
  } else if (!completed) {
    completed = true;
    list = posts.map((o) => `<${o.link}|${o.title}> - ${o.creator}`).join("\n");
    let payload = {
      channel: process.env.ENV == "production" ? "#2022-ironman" : "#test",
      text: `凹嗚嗚嗚嗚嗚嗚嗚！今天所有人都已完賽嚕！\n\n${list}\n\n今天可以安心睡覺了！！`,
    };
    await axios.post(process.env.SLACK_WEBHOOK_URL, payload);
  }
}
